# Paragraphs Limits README
Allows site builders to set minimum and maximum number of a
paragraph type allowed on a field.

## Installation / Configuration
Install as per any other Drupal Module. https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

## Functionality
- Adds a new 'Paragraphs with limits' entity selection plugin for paragraph
fields that allows users to set lower and upper limits for paragraphs.
- Defines a new constraint that validates these limits when a user submits
a paragraphs field.
- Setting a limit to '0' disables that limit.
- Reaching the upper limit of a paragraph will make that paragraph option
hide so no more can be added.
