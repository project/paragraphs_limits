<?php

/**
 * @file
 * Hook implementations for paragraphs_limits module.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Implements hook_entity_bundle_field_info_alter().
 *
 * @todo WARNING: This hook will be changed in https://www.drupal.org/node/2346347.
 */
function paragraphs_limits_entity_bundle_field_info_alter(array &$fields, EntityTypeInterface $entity_type, string $bundle): void {
  // Add our paragraph limit constraint to all paragraph fields.
  foreach ($fields as $field) {
    if (isset($field->getSettings()['target_type'])
      && $field->getSettings()['target_type'] == 'paragraph'
      && isset($field->getSettings()['handler'])
      && $field->getSettings()['handler'] == 'paragraphs_limits'
    ) {
      $field->addConstraint('ParagraphsLimits', []);
    }
  }
}

/**
 * Implements hook_field_widget_complete_WIDGET_TYPE_form_alter().
 *
 * Disallow adding more than allowed number of paragraphs on the legacy widget.
 */
function paragraphs_limits_field_widget_complete_entity_reference_paragraphs_form_alter(array &$elements, FormStateInterface $form_state, array $context): void {
  _paragraphs_limits_disallow_limited_paragraphs($elements, $form_state, $context);
}

/**
 * Implements hook_field_widget_complete_WIDGET_TYPE_form_alter().
 *
 * Disallow adding more than allowed number of paragraphs on the stable widget.
 */
function paragraphs_limits_field_widget_complete_paragraphs_form_alter(array &$elements, FormStateInterface $form_state, array $context): void {
  _paragraphs_limits_disallow_limited_paragraphs($elements, $form_state, $context);
}

/**
 * Helper function to disallow paragraphs on both paragraphs widgets.
 */
function _paragraphs_limits_disallow_limited_paragraphs(array &$elements, FormStateInterface $form_state, array $context): void {
  // Get the field definition.
  $field_definition = $context['items']->getFieldDefinition();

  // We only want to act on fields handled by us.
  if (isset($field_definition->getSettings()['handler'])
    && $field_definition->getSettings()['handler'] == 'paragraphs_limits'
  ) {
    $paragraphs_added = [];
    foreach (Element::children($elements['widget']) as $element) {
      if (is_numeric($element)) {
        $paragraphs_added[$elements['widget'][$element]['#paragraph_type']][] = $elements['widget'][$element];
      }
    }

    // Get the paragraph field settings.
    $handler_settings = $field_definition->getSettings()['handler_settings']['target_bundles_drag_drop'];
    $handler = \Drupal::service('plugin.manager.entity_reference_selection')->getSelectionHandler($field_definition);
    $allowed_bundles = $handler->getSortedAllowedTypes();
    $limited_allowed_bundles = array_intersect_key($handler_settings, $allowed_bundles);

    foreach ($limited_allowed_bundles as $paragraph_type_id => $paragraph_settings) {
      $count = !empty($paragraphs_added[$paragraph_type_id])
        ? count($paragraphs_added[$paragraph_type_id])
        : 0;

      if ((int) $paragraph_settings['upper_limit'] > 0
        && $count >= $paragraph_settings['upper_limit']) {
        // Support for select.
        unset($elements['widget']['add_more']['add_more_select']['#options'][$paragraph_type_id]);
        // Support for drop button.
        unset($elements['widget']['add_more']['operations']['#links']['add_more_button_' . $paragraph_type_id]);
        // Support for modal buttons.
        unset($elements['widget']['add_more']['add_more_button_' . $paragraph_type_id]);
      }
    }

    // If all actions were removed we have nothing to add.
    // Remove the add_more element completely.
    if (_paragraphs_limits_add_more_is_empty($elements['widget']['add_more'] ?? [])) {
      unset($elements['widget']['add_more']);
    }
  }
}

/**
 * Check if the add_more element has paragraphs to add or not.
 *
 * @param array<string,mixed> $element
 *   The add more element.
 *
 * @return bool
 *   True if empty.
 */
function _paragraphs_limits_add_more_is_empty(array $element): bool {
  // Check if select is empty.
  if (isset($element['add_more_select'])) {
    return empty($element['add_more_select']['#options']);
  }

  // Check if buttons are empty.
  foreach ($element as $key => $value) {
    if (strpos($key, 'add_more_button_') === 0) {
      return FALSE;
    }
  }

  return TRUE;
}
