<?php

namespace Drupal\paragraphs_limits\Plugin\EntityReferenceSelection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\EntityReferenceSelection\ParagraphSelection;

/**
 * Allows the addition of limits to the selected paragraphs.
 *
 * @EntityReferenceSelection(
 *   id = "paragraphs_limits",
 *   label = @Translation("Paragraphs with limits"),
 *   group = "paragraphs_limits",
 *   entity_types = {"paragraph"},
 *   weight = 1
 * )
 */
class ParagraphsLimitsSelection extends ParagraphSelection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $entity_type_id = $this->configuration['target_type'];
    $selection_handler_settings = $this->configuration ?: [];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

    foreach ($bundles as $bundle_name => $bundle_info) {
      $form['target_bundles_drag_drop'][$bundle_name]['lower_limit'] = [
        '#type' => 'number',
        '#min' => 0,
        '#step' => 1,
        '#default_value' => $selection_handler_settings['target_bundles_drag_drop'][$bundle_name]['lower_limit'] ?? 0,
      ];

      $form['target_bundles_drag_drop'][$bundle_name]['upper_limit'] = [
        '#type' => 'number',
        '#min' => 0,
        '#step' => 1,
        '#default_value' => $selection_handler_settings['target_bundles_drag_drop'][$bundle_name]['upper_limit'] ?? 0,
      ];
    }

    $form['target_bundles_drag_drop']['#header'][] = $this->t('Lower limit');
    $form['target_bundles_drag_drop']['#header'][] = $this->t('Upper limit');

    return $form;
  }

}
