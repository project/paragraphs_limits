<?php

namespace Drupal\paragraphs_limits\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs\Plugin\EntityReferenceSelection\ParagraphSelection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ParagraphsLimits constraint.
 */
class ParagraphsLimitsConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity reference selection plugin manager.
   */
  protected SelectionPluginManager $selectionManager;

  /**
   * Provides discovery and retrieval of entity type bundles.
   */
  protected EntityTypeBundleInfoInterface $entityBundleInfo;

  /**
   * Constructs a ValidReferenceConstraintValidator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager $selection_manager
   *   The entity selection manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SelectionPluginManager $selection_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->selectionManager = $selection_manager;
    $this->entityBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $item = $items->first();
    if (!$item) {
      return;
    }

    // Get the referenced paragraphs.
    $paragraphs = $items->referencedEntities();

    // Get the field definition.
    $field_definition = $items->getFieldDefinition();

    // Get the paragraph field settings.
    $settings = $field_definition->getSettings()['handler_settings']['target_bundles_drag_drop'];

    $handler = $this->selectionManager->getSelectionHandler($field_definition);
    if ($handler instanceof ParagraphSelection) {
      $allowed_bundles = $handler->getSortedAllowedTypes();
    }

    $paragraphs = $this->filterParagraphs($paragraphs, $allowed_bundles ?? [], $settings);

    // A paragraph should not be added to a field more than its limit allows.
    foreach ($paragraphs as $paragraph) {
      if (isset($paragraph['items'])) {
        $count = count($paragraph['items']);
      }
      else {
        $count = 0;
      }

      // Get the form display. Needed to get the field widget.
      $form_display = $this->entityTypeManager
        ->getStorage('entity_form_display')
        ->load(
          $field_definition->getTargetEntityTypeId() .
          '.' .
          $field_definition->getTargetBundle() .
          '.default'
        );

      // Get the field widget.
      $field_widget = $form_display->getComponent($field_definition->getName());

      if ((int) $paragraph['upper_limit'] != 0 && $count > (int) $paragraph['upper_limit']) {
        $this->context->addViolation($constraint->upperLimitMessage, [
          '%limit' => $paragraph['upper_limit'],
          '%title' => ((int) $paragraph['upper_limit'] > 1) ? $field_widget['settings']['title_plural'] : $field_widget['settings']['title'],
          '%paragraph_type' => $paragraph['label'],
        ]);
      }

      if ((int) $paragraph['lower_limit'] != 0 && $count < (int) $paragraph['lower_limit']) {
        $this->context->addViolation($constraint->lowerLimitMessage, [
          '%limit' => $paragraph['lower_limit'],
          '%title' => ((int) $paragraph['lower_limit'] > 1) ? $field_widget['settings']['title_plural'] : $field_widget['settings']['title'],
          '%paragraph_type' => $paragraph['label'],
        ]);
      }
    }
  }

  /**
   * Gets the limit settings for each paragraph bundle on a field.
   *
   * Returns an array with only the allowed paragraphs and their limits.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph[] $paragraphs
   *   An array of paragraph entities.
   * @param string[] $allowed_bundles
   *   An array of allowed paragraph bundles keyed by bundle ID.
   * @param array<string, mixed> $settings
   *   An array of settings including limits for each paragraph bundle.
   *
   * @return array<string, mixed>
   *   An array with the allowed paragraphs and their limits.
   */
  protected function filterParagraphs(array $paragraphs, array $allowed_bundles, array $settings): array {
    $limited_paragraphs = [];

    // Settings has all paragraphs on a field with their limits.
    // We only need the allowed ones.
    $limited_allowed_bundles = array_intersect_key($settings, $allowed_bundles);

    // We need the bundle info to get the label the bundle given its bundle ID.
    $paragraph_bundles = $this->entityBundleInfo->getBundleInfo('paragraph');

    foreach ($limited_allowed_bundles as $paragraph_type_id => $paragraph_settings) {
      $limited_paragraphs[$paragraph_type_id]['label'] = $paragraph_bundles[$paragraph_type_id]['label'];
      $limited_paragraphs[$paragraph_type_id]['lower_limit'] = $settings[$paragraph_type_id]['lower_limit'];
      $limited_paragraphs[$paragraph_type_id]['upper_limit'] = $settings[$paragraph_type_id]['upper_limit'];
    }

    foreach ($paragraphs as $paragraph) {
      $paragraph_type = $paragraph->getParagraphType();
      $paragraph_type_id = $paragraph_type->id();

      $limited_paragraphs[$paragraph_type_id]['items'][] = $paragraph;
    }

    return $limited_paragraphs;
  }

}
