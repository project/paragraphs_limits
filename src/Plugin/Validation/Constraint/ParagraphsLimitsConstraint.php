<?php

namespace Drupal\paragraphs_limits\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a limit is set on the paragraph.
 *
 * @Constraint(
 *   id = "ParagraphsLimits",
 *   label = @Translation("Paragraphs Limits", context = "Validation"),
 * )
 */
class ParagraphsLimitsConstraint extends Constraint {

  /**
   * Message shown when more paragraphs of a type than allowed are added.
   */
  public string $upperLimitMessage = 'A maximum of %limit "%title" of type "%paragraph_type" is allowed.';

  /**
   * Message shown when fewer paragraphs of a type than allowed are added.
   */
  public string $lowerLimitMessage = 'A minimum of %limit "%title" of type "%paragraph_type" is allowed.';

}
