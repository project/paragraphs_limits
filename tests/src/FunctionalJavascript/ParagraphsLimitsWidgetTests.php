<?php

namespace Drupal\Tests\paragraphs_limits\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;
use Drupal\Tests\paragraphs\FunctionalJavascript\LoginAdminTrait;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;
use Drupal\Tests\paragraphs\Traits\ParagraphsCoreVersionUiTestTrait;

/**
 * Test paragraphs limits user interface.
 *
 * @group paragraphs_limits
 */
class ParagraphsLimitsWidgetTests extends WebDriverTestBase {

  use FieldUiTestTrait;
  use LoginAdminTrait;
  use ParagraphsTestBaseTrait;
  use ParagraphsCoreVersionUiTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'paragraphs',
    'paragraphs_limits',
    'field',
    'field_ui',
    'block',
    'file',
    'system',
    'entity_reference_revisions',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->placeDefaultBlocks();
  }

  /**
   * Tests paragraphs_limits with stable widget.
   *
   * @param string $widget_type
   *   The widget type to test.
   * @param string $add_mode
   *   The add mode to test.
   * @param int $lower_limit
   *   The lower limit to test.
   * @param int $upper_limit
   *   The upper limit to test.
   * @param int $cardinality
   *   The field storage limit to test.
   *
   * @dataProvider providerData
   */
  public function testParagraphsWidgets(string $widget_type, string $add_mode, int $lower_limit, int $upper_limit, int $cardinality = NULL): void {
    $assert_session = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();

    $this->addParagraphedContentType('landing_page');
    $this->loginAsAdmin([
      'administer content types',
      'administer node form display',
      'edit any landing_page content',
      'create landing_page content',
    ]);

    // Load the form display configuration for the landing_page.
    $form_display = EntityFormDisplay::load('node.landing_page.default');
    // Get the widget settings for the paragraphs field and set the add mode.
    $widget_settings = $form_display->getComponent('field_paragraphs');
    $widget_settings['type'] = $widget_type;
    $widget_settings['settings']['add_mode'] = $add_mode;
    $form_display->setComponent('field_paragraphs', $widget_settings);
    $form_display->save();

    // Add a Paragraph type.
    $paragraph_type = 'text_paragraph';
    $this->addParagraphsType($paragraph_type);

    // Load the field configuration for the landing_page paragraphs field.
    $field_config = FieldConfig::loadByName('node', 'landing_page', 'field_paragraphs');
    // Handler settings for our paragraphs field.
    $handler_settings = [
      'target_bundles_drag_drop' => [
        'text_paragraph' => [
          'lower_limit' => $lower_limit,
          'upper_limit' => $upper_limit,
        ],
      ],
    ];
    // Switch the handler to the paragraph_limits handler and set its settings.
    $field_config->setSetting('handler', 'paragraphs_limits');
    $field_config->setSetting('handler_settings', $handler_settings);
    $field_config->save();

    if ($cardinality) {
      /** @var \Drupal\field\FieldStorageConfigInterface $field_storage */
      $field_storage = FieldStorageConfig::load('node.field_paragraphs');
      $field_storage->setCardinality($cardinality)->save();
    }

    $this->drupalGet('node/add/landing_page');
    $page->fillField('Title', 'Page title');

    // Add as many paragraphs as the upper limit allows.
    // One paragraph should already exist on the page so start at 1.
    for ($i = 1; $i < $upper_limit; $i++) {
      if ($add_mode == 'select') {
        $page->find('css', '[name="field_paragraphs_add_more"]')->click();
        $assert_session->assertWaitOnAjaxRequest();
      }
      elseif ($add_mode == 'dropdown' || $add_mode == 'button') {
        $assert_session->waitForField('field_paragraphs_' . $paragraph_type . '_add_more');
        $page->find('css', '[name="field_paragraphs_' . $paragraph_type . '_add_more"]')->click();
        $assert_session->assertWaitOnAjaxRequest();
      }
      elseif ($add_mode == 'modal') {
        $assert_session->waitForField('edit-field-paragraphs-add-more-add-modal-form-area-add-more');
        $page->findButton('Add Paragraph')->click();
        $assert_session->elementTextContains('css', '.ui-dialog-title', 'Add Paragraph');
        $paragraphs_dialog = $this->assertSession()->waitForElementVisible('css', 'div.ui-dialog');
        $paragraphs_dialog->pressButton('text_paragraph');
        $assert_session->assertWaitOnAjaxRequest();
      }
    }

    // Check that once if limit is reached the add more buttons aren't shown.
    switch ($add_mode) {
      case 'select':
        $this->assertEmpty($page->find('css', '[name="field_paragraphs_add_more"]'));
        break;

      case 'dropdown':
      case 'button':
        $this->assertEmpty($page->find('css', '[name="field_paragraphs_' . $paragraph_type . '_add_more"]'));
        break;

      case 'modal':
        $this->createScreenshot(\Drupal::root() . '/sites/default/files/simpletest/screen.png');
        $this->assertEmpty($page->find('css', '[data-drupal-selector="edit-field-paragraphs-add-more-add-modal-form-area-add-more"]'));
        break;
    }
  }

  /**
   * Data provider for testParagraphsWidgets().
   *
   * @return \Generator
   *   Test cases.
   */
  public static function providerData(): \Generator {
    foreach ([NULL, 3] as $cardinality) {
      $limit_text = $cardinality ? " w/limit {$cardinality}" : '';

      yield "Entity Reference Paragraphs (legacy widget{$limit_text}) with select" => [
        'entity_reference_paragraphs', 'select', 2, 3, $cardinality,
      ];
      yield "Entity Reference Paragraphs (legacy widget{$limit_text}) with button" => [
        'entity_reference_paragraphs', 'button', 2, 3, $cardinality,
      ];
      yield "Entity Reference Paragraphs (legacy widget{$limit_text}) with dropdown" => [
        'entity_reference_paragraphs', 'dropdown', 2, 3, $cardinality,
      ];

      yield "Paragraphs (stable widget{$limit_text}) with select" => [
        'paragraphs', 'select', 2, 3, $cardinality,
      ];
      yield "Paragraphs (stable widget{$limit_text}) with button" => [
        'paragraphs', 'button', 2, 3, $cardinality,
      ];
      yield "Paragraphs (stable widget{$limit_text}) with dropdown" => [
        'paragraphs', 'dropdown', 2, 3, $cardinality,
      ];
      yield "Paragraphs (stable widget{$limit_text}) with modal" => [
        'paragraphs', 'modal', 2, 3, $cardinality,
      ];
    }
  }

}
