<?php

namespace Drupal\Tests\paragraphs_limits\Kernel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Tests the uninstall hook functionality of the paragraphs_limits module.
 *
 * @group paragraphs_limits
 */
class ParagraphsLimitsUninstallTest extends ParagraphsLimitsKernelTestBase {

  /**
   * The entity type manager service.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module installer service.
   */
  protected ModuleInstallerInterface $moduleInstaller;

  /**
   * The module handler service.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->moduleInstaller = $this->container->get('module_installer');
    $this->moduleHandler = $this->container->get('module_handler');
  }

  /**
   * Test uninstalling the paragraphs_limits module.
   */
  public function testUninstall(): void {
    // Create the "article" content type.
    NodeType::create([
      'type' => 'article',
      'name' => 'Article',
    ])->save();

    // Create a test paragraphs type.
    $paragraphs_type = ParagraphsType::create([
      'id' => 'test_type',
      'label' => 'Test Type',
    ]);
    $paragraphs_type->save();

    // Create a test field with paragraphs_limits handler.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_test',
      'entity_type' => 'node',
      'type' => 'entity_reference_revisions',
      'settings' => [
        'target_type' => 'paragraph',
      ],
    ]);
    $field_storage->save();

    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'article',
      'label' => 'Test Field',
      'settings' => [
        'handler' => 'paragraphs_limits',
        'handler_settings' => [
          'target_bundles' => ['test_type' => 'test_type'],
          'target_bundles_drag_drop' => [
            'test_type' => [
              'lower_limit' => 1,
              'upper_limit' => 10,
            ],
          ],
        ],
      ],
    ]);
    $field_config->save();

    // Verify field handler before uninstallation.
    $this->assertEquals('paragraphs_limits', $field_config->getSetting('handler'));

    // Uninstall the module.
    $this->moduleInstaller->uninstall(['paragraphs_limits']);

    // Verify the module is uninstalled.
    $this->assertFalse($this->moduleHandler->moduleExists('paragraphs_limits'));

    // Reload the field config.
    $field_config = FieldConfig::load($field_config->id());
    $handler_settings = $field_config->getSetting('handler_settings');

    // Verify the handler is reset to default:paragraph.
    $this->assertEquals('default:paragraph', $field_config->getSetting('handler'));

    // Verify the upper and lower limits were removed.
    $this->assertArrayNotHasKey('lower_limit', $handler_settings['target_bundles_drag_drop']['test_type']);
    $this->assertArrayNotHasKey('upper_limit', $handler_settings['target_bundles_drag_drop']['test_type']);

    // Verify other settings are retained.
    $this->assertEquals(['test_type' => 'test_type'], $handler_settings['target_bundles']);
  }

}
