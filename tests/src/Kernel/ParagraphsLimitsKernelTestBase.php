<?php

namespace Drupal\Tests\paragraphs_limits\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Provides a base class for paragraphs_limits kernel tests.
 */
abstract class ParagraphsLimitsKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'paragraphs_limits',
    'paragraphs',
    'node',
    'field',
    'system',
    'entity_reference_revisions',
    'user',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('node');
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('paragraph');
    $this->installSchema('node', ['node_access']);
  }

}
