<?php

namespace Drupal\Tests\paragraphs_limits\Kernel\Plugin\Validation\Constraint;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs_limits\Plugin\Validation\Constraint\ParagraphsLimitsConstraint;
use Drupal\paragraphs_limits\Plugin\Validation\Constraint\ParagraphsLimitsConstraintValidator;
use Drupal\Tests\paragraphs_limits\Kernel\ParagraphsLimitsKernelTestBase;

/**
 * Tests the ParagraphsLimitsConstraintValidator.
 *
 * @group paragraphs_limits
 */
class ParagraphsLimitsConstraintValidatorTest extends ParagraphsLimitsKernelTestBase {

  /**
   * The constraint validator.
   */
  protected ParagraphsLimitsConstraintValidator $validator;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Instantiate the validator.
    $this->validator = new ParagraphsLimitsConstraintValidator(
      $this->container->get('entity_type.manager'),
      $this->container->get('plugin.manager.entity_reference_selection'),
      $this->container->get('entity_type.bundle.info')
    );

    // Create the content type.
    $node_type = NodeType::create([
      'type' => 'landing_page',
      'name' => 'Landing Page',
    ]);
    $node_type->save();

    // Create the paragraph type.
    $paragraphs_type = ParagraphsType::create([
      'id' => 'text_paragraph',
      'label' => 'Text Paragraph',
    ]);
    $paragraphs_type->save();

    // Handler settings for our paragraphs field.
    $handler_settings = [
      'target_bundles_drag_drop' => [
        'text_paragraph' => [
          'lower_limit' => 2,
          'upper_limit' => 3,
        ],
      ],
    ];

    // Add a paragraphs field.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_paragraphs',
      'entity_type' => 'node',
      'type' => 'entity_reference_revisions',
      'cardinality' => FieldStorageConfig::CARDINALITY_UNLIMITED,
      'settings' => [
        'target_type' => 'paragraph',
      ],
    ]);
    $field_storage->save();

    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'landing_page',
      'settings' => [
        'handler' => 'paragraphs_limits',
        'handler_settings' => $handler_settings,
      ],
    ])->save();

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('node', 'landing_page');
    $form_display = $form_display->setComponent('field_paragraphs', ['type' => 'paragraphs']);
    $form_display->save();

    $view_display = \Drupal::service('entity_display.repository')->getViewDisplay('node', 'landing_page');
    $view_display->setComponent('field_paragraphs', ['type' => 'entity_reference_revisions_entity_view']);
    $view_display->save();
  }

  /**
   * Tests paragraph limits validation logic.
   */
  public function testValidateParagraphsUpperLimits(): void {
    // Create 4 paragraphs when 3 are expected.
    $paragraphs = [];
    for ($i = 1; $i <= 4; $i++) {
      $paragraph = Paragraph::create([
        'type' => 'text_paragraph',
        'field_text' => 'Example text ' . $i,
      ]);
      $paragraph->save();
      $paragraphs[] = $paragraph;
    }

    // Create a node with paragraphs.
    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'landing_page',
      'field_paragraphs' => $paragraphs,
    ]);
    $node->save();

    // Create the constraint.
    $constraint = new ParagraphsLimitsConstraint();

    // Validate the node paragraphs field.
    $violations = $node->validate();

    // Assert violations are not empty.
    $this->assertNotEmpty($violations);
    // Get paragraph field violations.
    $paragraphs_field_violations = $violations->getByField('field_paragraphs');
    $this->assertNotEmpty($paragraphs_field_violations);
    $this->assertCount(1, $paragraphs_field_violations);
    // Get the violation.
    $violation = $violations[0];
    $parameters = $violation->getParameters();
    $this->assertEquals($constraint->upperLimitMessage, $violation->getMessageTemplate());
    $this->assertEquals(3, $parameters['%limit']);
    $this->assertEquals('Paragraphs', $parameters['%title']);
    $this->assertEquals('Text Paragraph', $parameters['%paragraph_type']);
    $expected_message = sprintf(
      'A maximum of %s "%s" of type "%s" is allowed.',
      $parameters['%limit'],
      $parameters['%title'],
      $parameters['%paragraph_type']
    );
    $this->assertEquals($expected_message, $violation->getMessage());
  }

  /**
   * Tests paragraph limits validation logic.
   */
  public function testValidateParagraphsLowerLimits(): void {
    // Create 1 paragraph when 2 are expected.
    $paragraphs = [];
    for ($i = 1; $i <= 1; $i++) {
      $paragraph = Paragraph::create([
        'type' => 'text_paragraph',
        'field_text' => 'Example text ' . $i,
      ]);
      $paragraph->save();
      $paragraphs[] = $paragraph;
    }

    $node = Node::create([
      'title' => 'Test Node',
      'type' => 'landing_page',
      'field_paragraphs' => $paragraphs,
    ]);
    $node->save();

    // Create the constraint.
    $constraint = new ParagraphsLimitsConstraint();

    // Validate the node paragraphs field.
    $violations = $node->validate();

    // Assert violations are not empty.
    $this->assertNotEmpty($violations);
    // Get paragraph field violations.
    $paragraphs_field_violations = $violations->getByField('field_paragraphs');
    $this->assertNotEmpty($paragraphs_field_violations);
    $this->assertCount(1, $paragraphs_field_violations);
    // Get the violation.
    $violation = $violations[0];
    $parameters = $violation->getParameters();
    $this->assertEquals($constraint->lowerLimitMessage, $violation->getMessageTemplate());
    $this->assertEquals(2, $parameters['%limit']);
    $this->assertEquals('Paragraphs', $parameters['%title']);
    $this->assertEquals('Text Paragraph', $parameters['%paragraph_type']);
    $expected_message = sprintf(
      'A minimum of %s "%s" of type "%s" is allowed.',
      $parameters['%limit'],
      $parameters['%title'],
      $parameters['%paragraph_type']
    );
    $this->assertEquals($expected_message, $violation->getMessage());
  }

}
